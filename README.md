### Описание задания

Модифицировать роль в прошлого задания
1. Добавить Юзера на каждый из серверов 
Name Ansible uid 1005 gid 1005
2. Сменить Имя серверов на test1 test2
3. Изменить файл hosts - добавить наши записи
4. Модулем setup вывести на экран ip настройки ваших удалённых серверов

### Ответ на задание

В плейбуке последовательно добавил новую группу и пользователя Ansible.
Для новых имён серверов в *vars/main.yml* в роли добавил новую переменную *hostnames* со списком [test1, test2].

При переименовании хостов использую индекс вхождения текущего *inventory_hostname* в список хостов в группе и сопоставляю этот индекс переменной *hostnames*.

Перед изменением файлов *hosts* собираю факты в отдельную переменную списком словарей, в котором будут hostname и ip-адрес каждого сервера.
Добавляю или изменяю строки в файлах hosts с помощью модуля *lineinfile*. При этом параметром *regexp* проверяю наличие строки с нужным hostname в файле, чтобы не дублировать записи.

Для вывода ip настроек вызываю модуль *setup* c фильтром *'ansible_default_ipv4'* и вывожу соответствующую переменную из фактов. То же самое можно получить запуском из командной строки:

**ansible all -m setup -a 'filter=ansible_default_ipv4'**
